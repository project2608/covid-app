package com.bonsay.covidapp;

import com.bonsay.covidapp.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Profile("!test")
@Component
public class MyRunner implements CommandLineRunner {

    @Autowired
    private ReportService reportService;

    @Override
    public void run(String... args) throws Exception {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("""
                    Welcome to covid info app!
                    Enter country name:\s""");
            var countryName = scanner.nextLine();
            reportService.makeCovidReport(countryName);
            System.out.println("--------------------");
        }

    }
}
