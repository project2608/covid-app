package com.bonsay.covidapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AllDto implements Serializable {
    private Integer confirmed;
    private Integer recovered;
    private Integer deaths;
    private Integer population;
    private Integer people_vaccinated;
    private Map<Date, Integer> dates;

}
