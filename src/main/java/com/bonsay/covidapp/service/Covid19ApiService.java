package com.bonsay.covidapp.service;

import com.bonsay.covidapp.dto.AllDto;

public interface Covid19ApiService {
    AllDto getCases(String country) throws Exception;
    AllDto getVaccinedInfo(String country) throws Exception;

    AllDto getHistoryInfo(String country) throws Exception;
}
