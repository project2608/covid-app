package com.bonsay.covidapp.service.impl;

import com.bonsay.covidapp.service.Covid19ApiService;
import com.bonsay.covidapp.service.ReportService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class ReportServiceImpl implements ReportService {

    private final Covid19ApiService covid19ApiService;

    public ReportServiceImpl(Covid19ApiService covid19ApiService) {
        this.covid19ApiService = covid19ApiService;
    }

    @Override
    public void makeCovidReport(String country) throws Exception {
        var info = covid19ApiService.getCases(country);
        var vaccinedInfo = covid19ApiService.getVaccinedInfo(country);
        var historyInfo = covid19ApiService.getHistoryInfo(country);
        var newCases = calcNewCases(historyInfo.getDates(), info.getConfirmed());

        System.out.println("""
                confirmed: %s
                recovered: %s
                deaths: %s
                vaccinated level: %s
                new confirmed cases: %s""".formatted(info.getConfirmed(), info.getRecovered(), info.getDeaths(),
                calcPercentage(vaccinedInfo.getPeople_vaccinated(), vaccinedInfo.getPopulation()), newCases));
    }

    public String calcPercentage(int vaccinated, int population) {
        return String.format("%.2f", (vaccinated * 100.0f) / population) + "%";
    }

    public int calcNewCases(Map<Date, Integer> dates, Integer confirmed) {
        var dates1 = dates.keySet().stream().toList();
        return confirmed - dates.get(dates1.get(0));
    }
}
