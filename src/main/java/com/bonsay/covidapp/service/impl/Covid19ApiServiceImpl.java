package com.bonsay.covidapp.service.impl;

import com.bonsay.covidapp.dto.AllDto;
import com.bonsay.covidapp.service.Covid19ApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.core.internal.LoadingCache;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class Covid19ApiServiceImpl implements Covid19ApiService {

    @Value("${covid.api.url}")
    private String baseUrl;

    private final RestTemplate restTemplate;

    public Covid19ApiServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public AllDto getCases(String country) throws Exception {
        try {
            var map = new HashMap<>();
            return mapResponse(restTemplate.getForObject(baseUrl + "/cases?country=" + country, map.getClass()));
        } catch (Exception ex) {
            throw new Exception(ex.getMessage(), ex);
        }
        
    }

    @Override
    public AllDto getVaccinedInfo(String country) throws Exception {
        try {
            var map = new HashMap<>();
            return mapResponse(restTemplate.getForObject(baseUrl + "/vaccines?country=" + country, map.getClass()));
        } catch (Exception ex) {
            throw new Exception(ex.getMessage(), ex);
        }
    }

    @Override
    public AllDto getHistoryInfo(String country) throws Exception {
        try {
            var map = new HashMap<>();
            return mapResponse(restTemplate.getForObject(baseUrl + "/history?status=confirmed&country=" + country, map.getClass()));
        } catch (Exception ex) {
            throw new Exception(ex.getMessage(), ex);
        }
    }

    private AllDto mapResponse(Map<String, Object> responseMap) {
        AllDto allDto = new AllDto();
        if (responseMap != null && responseMap.get("All") != null) {
            var all = (Map<String, Object>) responseMap.get("All");
            var confirmed = (Integer) all.get("confirmed");
            var recovered = (Integer) all.get("recovered");
            var deaths = (Integer) all.get("deaths");
            var people_vaccinated = (Integer) all.get("people_vaccinated");
            var population = (Integer) all.get("population");
            var dates = (Map<Date, Integer>) all.get("dates");

            allDto.setConfirmed(confirmed);
            allDto.setRecovered(recovered);
            allDto.setDeaths(deaths);
            allDto.setPopulation(population);
            allDto.setPeople_vaccinated(people_vaccinated);
            allDto.setDates(dates);
            all.clear();
            responseMap.clear();
        }
        return allDto;
    }
}
