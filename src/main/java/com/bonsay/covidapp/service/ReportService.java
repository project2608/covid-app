package com.bonsay.covidapp.service;

public interface ReportService {
    void makeCovidReport(String country) throws Exception;
}
