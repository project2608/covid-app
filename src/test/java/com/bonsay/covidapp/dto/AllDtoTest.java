package com.bonsay.covidapp.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class AllDtoTest {

    private AllDto allDto;

    @BeforeEach
    public void setUp() throws Exception {
        allDto = new AllDto();
    }

    @Test
    void getDeaths() {
        allDto.setDeaths(0);
        assertEquals(0, allDto.getDeaths());
    }

    @Test
    void getPopulation() {
        allDto.setPopulation(3000);
        assertEquals(3000, allDto.getPopulation());
    }

    @Test
    void getPeople_vaccinated() {
        allDto.setPeople_vaccinated(100);
        assertEquals(100, allDto.getPeople_vaccinated());
    }

    @Test
    void getDates() {
        Map<Date, Integer> map = new HashMap<>();
        allDto.setDates(map);
        assertEquals(map, allDto.getDates());
    }

    @Test
    void setConfirmed() {
        allDto.setConfirmed(333);
        assertEquals(333, allDto.getConfirmed());
    }

    @Test
    void setRecovered() {
        allDto.setRecovered(44);
        assertEquals(44, allDto.getRecovered());
    }

    @Test
    void setDeaths() {
        allDto.setDeaths(0);
        assertEquals(0, allDto.getDeaths());
    }

    @Test
    void setPopulation() {
        allDto.setPopulation(999);
        assertEquals(999, allDto.getPopulation());
    }

    @Test
    void setPeople_vaccinated() {
        allDto.setPeople_vaccinated(777);
        assertEquals(777, allDto.getPeople_vaccinated());
    }

    @Test
    void setDates() {
        Map<Date, Integer> map = new HashMap<>();
        allDto.setDates(map);
        assertEquals(map, allDto.getDates());
    }
}