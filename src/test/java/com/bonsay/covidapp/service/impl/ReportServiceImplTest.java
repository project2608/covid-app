package com.bonsay.covidapp.service.impl;

import com.bonsay.covidapp.dto.AllDto;
import com.bonsay.covidapp.service.Covid19ApiService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

class ReportServiceImplTest {

    ReportServiceImpl reportService;

    @Mock
    Covid19ApiService covid19ApiService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        reportService = new ReportServiceImpl(covid19ApiService);
    }

    @Test
    public void makeCovidReport() throws Exception {
        String country = "Germany";
        covid19ApiService.getCases(country);
        covid19ApiService.getVaccinedInfo(country);
        covid19ApiService.getHistoryInfo(country);
        AllDto allDto = new AllDto();
        allDto.setConfirmed(12345);
        when(covid19ApiService.getCases(country)).thenReturn(allDto);

        assertEquals(12345, covid19ApiService.getCases(country).getConfirmed());
        verify(covid19ApiService, times(2)).getCases(country);
        verify(covid19ApiService, times(1)).getVaccinedInfo(country);
        verify(covid19ApiService, times(1)).getHistoryInfo(country);
    }

    @Test
    public void calcPercentage() {
        Integer vaccinated = 10;
        Integer population = 50;
        assertEquals("20,00%", reportService.calcPercentage(vaccinated, population));
    }

    @Test
    public void calcNewCases() {
        Map<Date, Integer> dates = new HashMap<>();
        dates.put(new Date(), 52);
        assertEquals(40, reportService.calcNewCases(dates, 92));
    }

}