package com.bonsay.covidapp.service.impl;

import com.bonsay.covidapp.dto.AllDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class Covid19ApiServiceImplTest {

    Covid19ApiServiceImpl covid19ApiService;

    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        covid19ApiService = new Covid19ApiServiceImpl(restTemplate);
    }

    @Test
    void getCases() throws Exception {
        var map = new HashMap<>();
        var country = "Germany";
        when(restTemplate.getForObject("https://covid-api.mmediagroup.fr/v1/cases?country=" + country, map.getClass())).thenReturn(null);
        assertEquals(new AllDto(), covid19ApiService.getCases(country));
    }

    @Test
    void getVaccinedInfo() throws Exception {
        var map = new HashMap<>();
        var country = "Germany";
        when(restTemplate.getForObject("https://covid-api.mmediagroup.fr/v1/vaccines?country=" + country, map.getClass())).thenReturn(null);
        assertEquals(new AllDto(), covid19ApiService.getVaccinedInfo(country));
    }

    @Test
    void getHistoryInfo() throws Exception {
        var map = new HashMap<>();
        var country = "Germany";
        when(restTemplate.getForObject("https://covid-api.mmediagroup.fr/v1/history?status=confirmed&country=" + country, map.getClass())).thenReturn(null);
        assertEquals(new AllDto(), covid19ApiService.getHistoryInfo(country));
    }

}